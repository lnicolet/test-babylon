package com.example.domain.models

data class CompanyDomainModel(val name: String, val catchPhrase: String, val bs: String)