package com.example.domain.models

data class GeoDomainModel(val lat: String, val lng: String)