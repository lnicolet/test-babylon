package com.example.data.entities

data class CompanyEntity(val name: String, val catchPhrase: String, val bs: String)