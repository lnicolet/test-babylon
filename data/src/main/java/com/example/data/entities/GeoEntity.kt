package com.example.data.entities

data class GeoEntity(val lat: String, val lng: String)