package com.example.babylon.postdetails.models

class PostDetail(val user: User, val commentList: List<Comment>)